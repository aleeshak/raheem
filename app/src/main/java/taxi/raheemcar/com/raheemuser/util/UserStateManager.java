package taxi.raheemcar.com.raheemuser.util;

import android.content.Context;

import taxi.raheemcar.com.raheemuser.database.DatabaseHelper;
import taxi.raheemcar.com.raheemuser.models.User;


/**
 * Created by Aleesha Kanwal on 29/07/2018.
 */

public class UserStateManager {

    User sLoggedInUser;
    private DatabaseHelper databaseHelper;

    private static final UserStateManager ourInstance = new UserStateManager();

    public static UserStateManager getInstance() {
        return ourInstance;
    }

    private UserStateManager() {
    }

    public User getsLoggedInUser(Context context) {
        databaseHelper = new DatabaseHelper(context);
        int size = databaseHelper.getAllUser().size()-1;
        if(databaseHelper.getAllUser().get(size)!=null)
            setsLoggedInUser(databaseHelper.getAllUser().get(size));
        return sLoggedInUser;
    }

    public void setsLoggedInUser(User sLoggedInUser) {
        this.sLoggedInUser = sLoggedInUser;
    }
}
