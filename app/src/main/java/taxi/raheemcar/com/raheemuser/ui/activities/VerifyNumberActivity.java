package taxi.raheemcar.com.raheemuser.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.models.MobileNo;
import taxi.raheemcar.com.raheemuser.models.Verify;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class VerifyNumberActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    TextView codeTV;
    AppCompatButton verifyBtn;
    int code;
    TextView resendCode;
    TextView number;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_number);
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
        verifyBtn = (AppCompatButton) findViewById(R.id.verify);
        codeTV = (TextView) findViewById(R.id.code);
        code = SharedPrefrences.getInt("code",this);
        codeTV.setText(String.valueOf(code));
        resendCode = (TextView) findViewById(R.id.forgotPasswordTV) ;
        number = (TextView) findViewById(R.id.number);

        String phone = SharedPrefrences.getString("phone",getApplicationContext());
        number.setText(phone);

        verifyBtn.setEnabled(false);
        final PinEntryEditText pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);


        resendCode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(resendCode.getText().toString().equals("Resend Code")){
                    registerNumber();
                }
                else {
                    SuperActivityToast.create(VerifyNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText("Please wait")
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                }

            }
        });

        new CountDownTimer(120000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                resendCode.setText("Resend Code in "+String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                resendCode.setText("Resend Code");
            }
        }.start();

        pinEntry.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                if (pinEntry != null) {

                    verifyBtn.setBackgroundResource(R.drawable.verfiy);
                    verifyBtn.setEnabled(true);

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });


        pinEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (pinEntry != null) {

                    pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                        @Override
                        public void onPinEntered(CharSequence str) {
                            if (str.toString().equals(String.valueOf(code))) {

                                verifyNumber();

                                // Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                            } else {

                                SuperActivityToast.create(VerifyNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                                        .setProgressBarColor(Color.WHITE)
                                        .setText("Failure in recognizing code")
                                        .setDuration(Style.DURATION_LONG)
                                        .setFrame(Style.FRAME_LOLLIPOP)
                                        .setColor(getResources().getColor(R.color.colorPrimary))
                                        .setAnimations(Style.ANIMATIONS_POP).show();

                               // Utility.showOkPopUp(VerifyNumberActivity.this,"Failure in recognizing code");
                                // Toast.makeText(getApplicationContext(), "FAIL", Toast.LENGTH_SHORT).show();
                                pinEntry.setText(null);
                            }
                        }
                    });
                }

            }
        });


    }

    public void verifyNumber() {

        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("verfying Mobile Number...");
        progressDialog.show();

        String phone = SharedPrefrences.getString("phone",this);
        Call<Verify> response = jsonApiInterface.verifyMobileNumber(code,phone);
        response.enqueue(new Callback<Verify>() {
            @Override
            public void onResponse(Call<Verify> call, Response<Verify> response) {
                Verify serverResponse = response.body();

                SharedPrefrences.putInt("code",code,getApplicationContext());

                if (serverResponse.getStatus()==1) {

                    progressDialog.cancel();
                    Utility.showOkPopUp(VerifyNumberActivity.this,serverResponse.getMessage());
                    SharedPrefrences.putString("id",serverResponse.getClientId(),getApplicationContext());
                    Intent intent = new Intent(getApplicationContext(),EmailRegisterActivity.class);
                    startActivity(intent);

                } else {


                    SuperActivityToast.create(VerifyNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(serverResponse.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                   // Utility.showOkPopUp(VerifyNumberActivity.this,serverResponse.getMessage());
                    progressDialog.cancel();

                }
            }

            @Override
            public void onFailure(Call<Verify> call, Throwable t) {
                t.getMessage();

                SuperActivityToast.create(VerifyNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

               // Utility.onServiceFailureNetworkCheck(VerifyNumberActivity.this,t.getMessage(),InternetConnection.isNetworkAvailable(VerifyNumberActivity.this));
                progressDialog.cancel();
                call.cancel();
            }
        });
    }

    public void registerNumber() {
        String phone = SharedPrefrences.getString("phone",getApplicationContext());
        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<MobileNo> response = jsonApiInterface.resgiterMobileNumber(phone);
        response.enqueue(new Callback<MobileNo>() {
            @Override
            public void onResponse(Call<MobileNo> call, Response<MobileNo> response) {
                MobileNo serverResponse = response.body();

                progressDialog.dismiss();

                int code = serverResponse.getCode();
                SharedPrefrences.putInt("code",code,getApplicationContext());

                SuperActivityToast.create(VerifyNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText("Code is resent successfully, please check your inbox")
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();


            }

            @Override
            public void onFailure(Call<MobileNo> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();

                SuperActivityToast.create(VerifyNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                call.cancel();
            }
        });
    }
}
