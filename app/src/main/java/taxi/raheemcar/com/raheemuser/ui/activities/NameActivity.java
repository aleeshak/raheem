package taxi.raheemcar.com.raheemuser.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.google.firebase.FirebaseApp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.constants.Constants;
import taxi.raheemcar.com.raheemuser.database.DatabaseHelper;
import taxi.raheemcar.com.raheemuser.models.SignupResponse;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class NameActivity extends AppCompatActivity {

    EditText passwordTV;
    AppCompatButton submit;
    private DatabaseHelper databaseHelper;
    JsonApiInterface jsonApiInterface;
    String name;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        passwordTV = (EditText) findViewById(R.id.passwordTV);
        submit = (AppCompatButton) findViewById(R.id.submit);
        databaseHelper = new DatabaseHelper(this);
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);


        submit.setEnabled(false);

        passwordTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                if(!Utility.isEmpty(passwordTV.getText().toString().trim())){
                    submit.setEnabled(true);
                    submit.setBackgroundResource(R.drawable.submit);
                }
                else {
                    submit.setBackgroundResource(R.drawable.disablesubmit);
                    submit.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        SharedPrefrences.putString("name",name,getApplicationContext());

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


             registerUser();

            }
        });

    }



    public void registerUser() {
        /**
         POST name and job Url encoded.
         **/

        if (FirebaseApp.getApps(this).isEmpty()) {
            FirebaseApp.initializeApp(this);
        }
        name = passwordTV.getText().toString().trim();

        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        String primary_key,email,password,token;

        primary_key = SharedPrefrences.getString("id",this);
        email = SharedPrefrences.getString("email",this);
        password = SharedPrefrences.getString("password",this);
        token = SharedPrefrences.getString(Constants.FIREBASE_TOKEN,this);

        Call<SignupResponse> response = jsonApiInterface.registerUser(primary_key,name,email,password,token);
        response.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                SignupResponse userList = response.body();

                if (userList.getObject() != null) {

                    progressDialog.cancel();
                    SharedPrefrences.putBoolean("login",true,getApplicationContext());
                    SharedPrefrences.getInstance().setUserObject(userList.getObject(),getApplicationContext());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);

                }
                else {



                    SuperActivityToast.create(NameActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(userList.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                 //   Toast.makeText(getApplicationContext(), userList.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();

                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();



                SuperActivityToast.create(NameActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

              //  Utility.onServiceFailureNetworkCheck(NameActivity.this,t.getMessage(),InternetConnection.isNetworkAvailable(NameActivity.this));
                call.cancel();
            }
        });
    }
}
