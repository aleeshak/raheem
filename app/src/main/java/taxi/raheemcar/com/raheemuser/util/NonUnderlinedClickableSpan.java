package taxi.raheemcar.com.raheemuser.util;

/**
 * Created by Ammar Amjad on 6/22/2015.
 */
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class NonUnderlinedClickableSpan extends ClickableSpan
{
    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(ds.linkColor);
        ds.setUnderlineText(false);
    }

    @Override
    public void onClick(View widget) {
        // TODO Auto-generated method stub
    }
}