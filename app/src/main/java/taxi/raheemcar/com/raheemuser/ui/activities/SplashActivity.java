package taxi.raheemcar.com.raheemuser.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;


public class SplashActivity extends Activity {
	private static int SPLASH_TIME_OUT = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {

				if(SharedPrefrences.getBoolean("login",getApplicationContext())) {
					Intent i = new Intent(SplashActivity.this, MainActivity.class);
					startActivity(i);
				}
				else {
					Intent i = new Intent(SplashActivity.this, GetStartedActivity.class);
					startActivity(i);
				}
				finish();
			}
		}, SPLASH_TIME_OUT);
	}

	@Override
	public void onBackPressed() {

	}
}
