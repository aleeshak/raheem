package taxi.raheemcar.com.raheemuser.networking;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import taxi.raheemcar.com.raheemuser.constants.BackendConstants;
import taxi.raheemcar.com.raheemuser.models.Drivers;
import taxi.raheemcar.com.raheemuser.models.Login;
import taxi.raheemcar.com.raheemuser.models.MobileNo;
import taxi.raheemcar.com.raheemuser.models.NewCaptain;
import taxi.raheemcar.com.raheemuser.models.POJO.Example;
import taxi.raheemcar.com.raheemuser.models.Rating;
import taxi.raheemcar.com.raheemuser.models.Reset;
import taxi.raheemcar.com.raheemuser.models.SignupResponse;
import taxi.raheemcar.com.raheemuser.models.Updatedposition;
import taxi.raheemcar.com.raheemuser.models.Verify;

/**
 * Created by Aleesha Kanwal
 */

public interface JsonApiInterface {

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.GET_DRIVERS)
    Call<Drivers> getDrivers(@Field("lat") String lat,@Field("lng") String lng,@Field("vehicle_id") String vehicle_id);


    @FormUrlEncoded
    @Headers({"Accept: application/json"})
    @POST(BackendConstants.MOBILE_NUMBER_REQUEST)
    Call<MobileNo> resgiterMobileNumber(@Field("mobile") String mobile);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.MOBILE_NUMBER_VERIFICATION)
    Call<Verify> verifyMobileNumber(@Field("code") Integer code,@Field("mobile") String mobile);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST(BackendConstants.LOGIN)
    Call<Login> login(@Field("mobile") String mobile,@Field("password") String password,@Field("token") String token);

    @FormUrlEncoded
    @POST(BackendConstants.REGISTER)
    Call<SignupResponse> registerUser(@Field("primary_key") String primary_key, @Field("name") String name, @Field("email") String email, @Field("password") String password, @Field("token") String token);


    @FormUrlEncoded
    @Headers({"Accept: application/json"})
    @POST(BackendConstants.RESET_PASSWORD)
    Call<Reset> resetPassword(@Field("email") String mobile);

    @FormUrlEncoded
    @POST(BackendConstants.UPDATED_POSITION)
    Call<Updatedposition> updatedPosition(@Field("driver_id") String driver_id_1,@Field("lat") String lat_1,@Field("lng") String lng_1);

    @FormUrlEncoded
    @POST(BackendConstants.REQUEST_NEW_CAPTAIN)
    Call<NewCaptain> requestNewCaptain(@Field("lat") double lat, @Field("lng") double lng, @Field("vehicle_id") String vehicle_id, @Field("client_id") String client_id, @Field("from") String from, @Field("to") String to);

    @GET("api/directions/json?key=AIzaSyDXDZuqzuntjqZgTGlmTYJAL0VX6BxwpeY")
    Call<Example> getDistanceDuration(@Query("units") String units, @Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode);

    @FormUrlEncoded
    @POST(BackendConstants.SAVE_CAPTAIN_RATING)
    Call<Rating> saveCaptainRating(@Field("rating") float rating, @Field("driver_id") String driver_id);
}
