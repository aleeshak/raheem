package taxi.raheemcar.com.raheemuser.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.github.rubensousa.bottomsheetbuilder.adapter.BottomSheetItemClickListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.constants.Constants;
import taxi.raheemcar.com.raheemuser.models.RideObject;
import taxi.raheemcar.com.raheemuser.models.Updatedposition;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.GPSTracker;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 17/09/2018.
 */
public class NotificationActivity extends AppCompatActivity implements OnMapReadyCallback , BottomSheetItemClickListener {
    private GoogleMap mMap;
    GPSTracker gpsTracker;
    JsonApiInterface jsonApiInterface;
    String driverName,carColor,carNumber,rating,time,imageName,driverNumber;
    ImageView driverImage;
    TextView driverNameTV,driverCarColor,driverCarNumber;
    RatingBar driverRating;
    String arrivalTime;
    TextView timeTV;
    AppCompatButton contactBtn,share;
    final Timer timer = new Timer();
    final Timer timerUser = new Timer();

    private boolean mShowingSimpleDialog;
    private BottomSheetMenuDialog mBottomSheetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        gpsTracker = new GPSTracker(getApplicationContext());
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);


        driverImage = (ImageView) findViewById(R.id.driverImage);
        driverNameTV = (TextView) findViewById(R.id.driverName);
        driverCarColor = (TextView) findViewById(R.id.driverCarColor);
        driverCarNumber = (TextView)findViewById(R.id.driverCarNumber);
        driverRating = (RatingBar) findViewById(R.id.driverRating);
        timeTV = (TextView) findViewById(R.id.time);
        contactBtn = (AppCompatButton) findViewById(R.id.contact);
        share = (AppCompatButton) findViewById(R.id.share);

        contactBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                BottomSheetMenuDialog dialog = new BottomSheetBuilder(NotificationActivity.this)
                        .setMode(BottomSheetBuilder.MODE_LIST)
                        .addTitleItem("Contact Captain")
                        .addItem(0, "Call", R.drawable.drivercall)
                        .addItem(1, "SMS", R.drawable.driversms)
                        .setItemClickListener(new BottomSheetItemClickListener() {
                            @Override
                            public void onBottomSheetItemClick(MenuItem item) {

                                if(item.getItemId()==0){
                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                    callIntent.setData(Uri.parse("tel:"+driverNumber));
                                    startActivity(callIntent);
                                }

                                if(item.getItemId()==1){

                                    String smsText = ".";

                                    Uri uri = Uri.parse("smsto:" + driverNumber);
                                    Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                                    intent.putExtra("sms_body", smsText);
                                    startActivity(intent);
                                }

                            }
                        })
                        .createDialog();

                dialog.show();
            }
        });

       share.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                RideObject object = SharedPrefrences.getInstance().getDriverObject();
                String captainName = object.getDriverName();
                String car =  object.getType();
                String vehicleNo =  object.getVehicleNo();
                String mobile = object.getDriverMobile();
                double lat =  gpsTracker.getLatitude();
                double lng = gpsTracker.getLongitude();

                String msg = "Hey I booked a Raheem! The captain is " + captainName + " , driving a " + car + " having number is " + mobile + " my current location is  \n" + " https://maps.google.com/?q="+lat +","+lng;
         /*
                String text = "Hey I booked a Raheem! The captain is SADEEQ RAHMAN , driving a CORROLA car   having number 6363 mobile number is 03349232440 my current location is \n" +
                        "https://maps.google.com/?q=34.7736562,72.3720533";
*/
                shareText(msg);
            }
        });

        driverName = SharedPrefrences.getInstance().getDriverObject().getDriverName();
        carColor = SharedPrefrences.getInstance().getDriverObject().getColor();
        carNumber = SharedPrefrences.getInstance().getDriverObject().getVehicleNo();
        rating = SharedPrefrences.getInstance().getDriverObject().getRating();
        time = SharedPrefrences.getInstance().getDriverObject().getDistance();
        imageName = SharedPrefrences.getInstance().getDriverObject().getImage();
        driverNumber = SharedPrefrences.getInstance().getDriverObject().getDriverMobile();

        Picasso.with(this).load(Constants.IMAGE_URL + imageName).placeholder(R.drawable.driver).into(driverImage);

        driverNameTV.setText(driverName);
        driverCarNumber.setText(carNumber);
        driverCarColor.setText(carColor);
        driverRating.setRating(Float.parseFloat(SharedPrefrences.getInstance().getDriverObject().getRating()));

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(SharedPrefrences.getBoolean("notif",getApplicationContext())){
            timeTV.setText("You are now on a Ride");
            SharedPrefrences.putBoolean("notif",false,getApplicationContext());
            SharedPrefrences.putBoolean("ride",true,getApplicationContext());
        }

        else {
            timer.schedule(new TimerTask() {

                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            updateDriverPosition();
                        }
                    });
                }
            }, 0, 10000);
        }

        if(SharedPrefrences.getBoolean("ride",getApplicationContext())) {
            timerUser.schedule(new TimerTask() {

                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            mMap.clear();
                            addUserLocationMarker(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                        }
                    });
                }
            }, 0, 10000);
        }

    }

    @Override
    protected void onDestroy() {
        timerUser.cancel();
        timer.cancel();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        timerUser.cancel();
        timer.cancel();
        super.onStop();
    }

    public void addUserLocationMarker(Double lat, Double lng) {

        //lat and long of driver here
        LatLng currentLocation = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title("You")
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.usertwo)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.5f));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

    }



    public void addLocationMarker(String lat,String lng) {

       /* latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();*/

        //lat and long of driver here
        LatLng currentLocation = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)))
                .title("Captain")
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.raheemmap)));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.5f));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

    }



    public void updateDriverPosition() {

        String driverID = SharedPrefrences.getInstance().getDriverObject().getDriverId();
        final String lat = SharedPrefrences.getInstance().getDriverObject().getStartLat();
        final String lng = SharedPrefrences.getInstance().getDriverObject().getStartLng();

        Log.e("position","updated");
        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));


        Call<Updatedposition> response = jsonApiInterface.updatedPosition(driverID, lat, lng);
        response.enqueue(new Callback<Updatedposition>() {
            @Override
            public void onResponse(Call<Updatedposition> call, Response<Updatedposition> response) {
                Updatedposition serverResponse = response.body();

                if (serverResponse.getStatus() == 1) {

                    mMap.clear();
                    arrivalTime =  serverResponse.getObject().getTime();
                    timeTV.setText("Your captain is " +arrivalTime + " Away");
                    addLocationMarker(serverResponse.getObject().getLat(), serverResponse.getObject().getLng());

                } else {

                    SuperActivityToast.create(NotificationActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(serverResponse.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                }

            }

            @Override
            public void onFailure(Call<Updatedposition> call, Throwable t) {
                t.getMessage();
                SuperActivityToast.create(NotificationActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

                call.cancel();
            }
        });
    }



    // Share text
    private void shareText(String text) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");// Plain format text

        // You can add subject also
        /*
         * sharingIntent.putExtra( android.content.Intent.EXTRA_SUBJECT,
         * "Subject Here");
         */

        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(sharingIntent, "Share Details Using"));
    }

    @Override
    public void onBottomSheetItemClick(MenuItem item) {

    }
}
