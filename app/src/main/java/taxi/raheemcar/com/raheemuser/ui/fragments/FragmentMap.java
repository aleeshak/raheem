package taxi.raheemcar.com.raheemuser.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.constants.Constants;
import taxi.raheemcar.com.raheemuser.models.Drivers;
import taxi.raheemcar.com.raheemuser.models.Vehicles;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.ui.activities.DropoffActivity;
import taxi.raheemcar.com.raheemuser.ui.adapters.VehiclesAdapter;
import taxi.raheemcar.com.raheemuser.ui.interfaces.ClickListenerAdapter;
import taxi.raheemcar.com.raheemuser.util.GPSTracker;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 16/08/2018.
 */

public class FragmentMap extends Fragment implements OnMapReadyCallback, ClickListenerAdapter {

    private GoogleMap mMap;
    private VehiclesAdapter mAdapter;
    private List<Vehicles> movieList = new ArrayList<>();
    JsonApiInterface jsonApiInterface;
    ProgressDialog progressDialog;
    LinearLayout linearLayoutGo,linearLayoutGoPlus,linearLayoutBike,linearLayoutRik;
    Marker mkr;
    GPSTracker gpsTracker;
    double latitude, longitude;
    static private FragmentMap _instance;
    ImageView go,goPlus,bike,rike;
    AppCompatButton rideNow;

    private static final int PERMISSION_REQUEST_CODE = 1;
    boolean isGpsTrue;
    public static final int PERMISSION_REQUEST_CODE_LOCATION =1;

    public static FragmentMap getInstance() {
        if (_instance == null) {
            synchronized (FragmentMap.class) {
                if (_instance == null)
                    _instance = new FragmentMap();
            }
        }
        return _instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, null);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        linearLayoutBike = (LinearLayout) rootView.findViewById(R.id.layoutBike);
        linearLayoutGo = (LinearLayout) rootView.findViewById(R.id.layoutGo);
        linearLayoutGoPlus = (LinearLayout) rootView.findViewById(R.id.layoutGoPlus);
        linearLayoutRik = (LinearLayout) rootView.findViewById(R.id.layoutRik);
        rideNow = (AppCompatButton) rootView.findViewById(R.id.rideNow);

        go = (ImageView) rootView.findViewById(R.id.iconGo);
        goPlus = (ImageView) rootView.findViewById(R.id.iconGoPlus);
        rike = (ImageView) rootView.findViewById(R.id.iconRik);
        bike = (ImageView) rootView.findViewById(R.id.iconBike);

        rideNow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), DropoffActivity.class);
                startActivity(intent);
            }
        });


        linearLayoutBike.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mMap.clear();

                displayDriversOnMap(Constants.VEHICLE_ID_BIKE);
                bike.setBackgroundResource(R.drawable.bikehighlighted);

                go.setBackgroundResource(R.drawable.economy0);
                goPlus.setBackgroundResource(R.drawable.luxury);
                rike.setBackgroundResource(R.drawable.rasksha);

            }
        });


        linearLayoutGo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mMap.clear();

                displayDriversOnMap(Constants.VEHICLE_ID_GO);
                go.setBackgroundResource(R.drawable.economyhighlighted0);

                goPlus.setBackgroundResource(R.drawable.luxury);
                bike.setBackgroundResource(R.drawable.bikee);
                rike.setBackgroundResource(R.drawable.rasksha);

            }
        });

        linearLayoutGoPlus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mMap.clear();
                displayDriversOnMap(Constants.VEHICLE_ID_GO_PLUS);
                goPlus.setBackgroundResource(R.drawable.luxuryhighlighted);

                bike.setBackgroundResource(R.drawable.bikee);
                rike.setBackgroundResource(R.drawable.rasksha);
                go.setBackgroundResource(R.drawable.economy0);
            }
        });

        linearLayoutRik.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mMap.clear();
                displayDriversOnMap(Constants.VEHICLE_ID_RAKSHAW);
                rike.setBackgroundResource(R.drawable.raskshahighlighted);

                go.setBackgroundResource(R.drawable.economy0);
                goPlus.setBackgroundResource(R.drawable.luxury);
                bike.setBackgroundResource(R.drawable.bikee);
            }
        });


        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);

        gpsTracker = new GPSTracker(getContext());

        mAdapter = new VehiclesAdapter(movieList);
        mAdapter.setClickListener(this);

        prepareVehiclesData();

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST_CODE);
            }
        }
        else{
            //load your map here
            mMap.setMyLocationEnabled(true);
        }


        if(gpsTracker.canGetLocation()) {

            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            displayDriversOnMap(Constants.VEHICLE_ID_GO);
            Log.e("aleesha","2");
        } else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            showSettingsAlert();

        }


       // displayDriversOnMap(Constants.VEHICLE_ID_GO);

    }


    public void addLocationMarker(){

            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

        LatLng currentLocation = new LatLng(latitude, longitude);

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng( latitude,longitude))
                .title("You")
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.raheemmap)));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.5f));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        //displayDriversOnMap(Constants.VEHICLE_ID_GO);


    }

    private void prepareVehiclesData() {
        Vehicles movie = new Vehicles("Go", R.drawable.economy0);
        movieList.add(movie);

        movie = new Vehicles("Rakshah", R.drawable.rasksha);
        movieList.add(movie);

        movie = new Vehicles("Go+", R.drawable.luxury);
        movieList.add(movie);

        movie = new Vehicles("Bike", R.drawable.bikee);
        movieList.add(movie);

        mAdapter.notifyDataSetChanged();
    }

    public void displayDriversOnMap(String vehicleID ) {

        SharedPrefrences.putString(Constants.SELECTED_VEHICLE_TYPE,vehicleID,getContext());

        if(vehicleID.equals(Constants.VEHICLE_ID_GO)){
           go.setBackgroundResource(R.drawable.economyhighlighted0);
        }

        addLocationMarker();

        Utility.onServiceSuccessNetworkCheck(getActivity(), InternetConnection.isNetworkAvailable(getActivity()));
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading Captains");
        progressDialog.show();

        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);

        gpsTracker = new GPSTracker(getContext());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();



        Call<Drivers> response = jsonApiInterface.getDrivers(String.valueOf(latitude),String.valueOf(longitude), vehicleID);
        response.enqueue(new Callback<Drivers>() {
            @Override
            public void onResponse(Call<Drivers> call, Response<Drivers> response) {
                Drivers serverResponse = response.body();

                if (serverResponse.getStatus()==1) {
                  progressDialog.dismiss();

                    for (int i = 0; i < serverResponse.getObject().size(); i++) {
                     //   MarkerOptions options = new MarkerOptions().position(new LatLng(Double.parseDouble(serverResponse.getObject().get(i).getLat()), Double.parseDouble(serverResponse.getObject().get(i).getLng())));
                        try {

                           mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(serverResponse.getObject().get(i).getLat()), Double.parseDouble(serverResponse.getObject().get(i).getLng())))
                                    .title("Captain")
                                    .snippet("S")
                                    //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                else {
                    progressDialog.dismiss();

                    SuperActivityToast.create(getActivity(), new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(serverResponse.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();
                }
            }

            @Override
            public void onFailure(Call<Drivers> call, Throwable t) {
                progressDialog.cancel();

                SuperActivityToast.create(getActivity(), new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

                call.cancel();
            }
        });
    }


    @Override
    public void onClick(View view, int position) {
        if(position ==0){ //go

            displayDriversOnMap(Constants.VEHICLE_ID_GO);
        }

        else if(position ==1){ //rakshaw

            displayDriversOnMap(Constants.VEHICLE_ID_RAKSHAW);
        }
        else if(position ==2){ //go+

            displayDriversOnMap(Constants.VEHICLE_ID_GO_PLUS);
        }
        else if(position ==3){ //bike

            displayDriversOnMap(Constants.VEHICLE_ID_BIKE);
        }
    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS settings");

        // Setting Dialog Message
        alertDialog.setMessage("Raheem would like to use your current location and GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);

                isGpsTrue = true;

             //  turnGPSOn();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                isGpsTrue = false;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }



    public static void requestPermission(String strPermission, int perCode, Context _c, Activity _a){

        if (ActivityCompat.shouldShowRequestPermissionRationale(_a,strPermission)){
            Toast.makeText(_a,"GPS permission allows us to access location data. Please allow in App Settings for additional functionality.",Toast.LENGTH_LONG).show();
        } else {

            ActivityCompat.requestPermissions(_a,new String[]{strPermission},perCode);
        }
    }

    public static boolean checkPermission(String strPermission,Context _c,Activity _a){
        int result = ContextCompat.checkSelfPermission(_c, strPermission);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void turnGPSOn(){
        String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            getActivity().sendBroadcast(poke);
        }
    }

    private void turnGPSOff(){
        String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(provider.contains("gps")){ //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            getActivity().sendBroadcast(poke);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        if(isGpsTrue){
            displayDriversOnMap(Constants.VEHICLE_ID_GO);
            isGpsTrue = false;
        }


    }


    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION)){

            Toast.makeText(getActivity(),"GPS permission allows us to access location data. Please allow in App Settings for additional functionality.",Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST_CODE);
        }
    }

 /*   @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Snackbar.make(getView(),"Permission Granted, Now you can access location data.",Snackbar.LENGTH_LONG).show();

                } else {

                    Snackbar.make(getView(),"Permission Denied, You cannot access location data.", Snackbar.LENGTH_LONG).show();

                }
                break;
        }
    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    // load your map here also

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
