package taxi.raheemcar.com.raheemuser.constants;

/**
 * Created by Aleesha Kanwal on 29/07/2018.
 */

public class Constants {

    public static final String FIREBASE_TOKEN = "firebasetoken";
    public static final String ADMIN_CHANNEL_ID = "1";
    public static final String VEHICLE_ID_GO = "1";
    public static final String VEHICLE_ID_RAKSHAW = "2";
    public static final String VEHICLE_ID_GO_PLUS = "4";
    public static final String VEHICLE_ID_BIKE = "3";
    public static final String SELECTED_VEHICLE_TYPE = "SELECTED_VEHICLE_TYPE";
    public static final String IMAGE_URL = "http://raheemcar.com/images/captains/";
}
