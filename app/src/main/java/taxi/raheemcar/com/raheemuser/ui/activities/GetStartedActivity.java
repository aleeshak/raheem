package taxi.raheemcar.com.raheemuser.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

import taxi.raheemcar.com.raheemuser.R;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class GetStartedActivity extends AppCompatActivity {

    AppCompatButton button;
    TextView startTv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
        button = (AppCompatButton) findViewById(R.id.mobileNoBtn);
        startTv = (TextView) findViewById(R.id.start);


     /*   SpannableString content = new SpannableString("Let's get Started!");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        startTv.setText(content);*/

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),MobileNumberActivity.class);
                startActivity(intent);

            }
        });
    }
}
