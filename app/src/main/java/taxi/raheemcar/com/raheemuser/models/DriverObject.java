
package taxi.raheemcar.com.raheemuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverObject {

    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("vehicle_id")
    @Expose
    private String vehicleId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("caption_email")
    @Expose
    private String captionEmail;
    @SerializedName("nic_front")
    @Expose
    private String nicFront;
    @SerializedName("nic_back")
    @Expose
    private String nicBack;
    @SerializedName("licence_front")
    @Expose
    private String licenceFront;
    @SerializedName("licence_back")
    @Expose
    private String licenceBack;
    @SerializedName("driver_mobile")
    @Expose
    private String driverMobile;
    @SerializedName("chassis_no")
    @Expose
    private String chassisNo;
    @SerializedName("vehicle_no")
    @Expose
    private String vehicleNo;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("radius")
    @Expose
    private String radius;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("joining_date")
    @Expose
    private String joiningDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("time")
    @Expose
    private String time;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCaptionEmail() {
        return captionEmail;
    }

    public void setCaptionEmail(String captionEmail) {
        this.captionEmail = captionEmail;
    }

    public String getNicFront() {
        return nicFront;
    }

    public void setNicFront(String nicFront) {
        this.nicFront = nicFront;
    }

    public String getNicBack() {
        return nicBack;
    }

    public void setNicBack(String nicBack) {
        this.nicBack = nicBack;
    }

    public String getLicenceFront() {
        return licenceFront;
    }

    public void setLicenceFront(String licenceFront) {
        this.licenceFront = licenceFront;
    }

    public String getLicenceBack() {
        return licenceBack;
    }

    public void setLicenceBack(String licenceBack) {
        this.licenceBack = licenceBack;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
