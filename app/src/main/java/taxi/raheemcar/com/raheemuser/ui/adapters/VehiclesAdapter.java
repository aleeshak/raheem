package taxi.raheemcar.com.raheemuser.ui.adapters;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.models.Vehicles;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.ui.interfaces.ClickListenerAdapter;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class VehiclesAdapter extends RecyclerView.Adapter<VehiclesAdapter.MyViewHolder> {

    private List<Vehicles> moviesList;
    ClickListenerAdapter clickListenerAdapter;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        private ImageView icon;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.titleVehicle);
            icon = (ImageView) view.findViewById(R.id.iconVehicle);

        }
    }


    public VehiclesAdapter(List<Vehicles> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehicles_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Vehicles movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        holder.icon.setBackgroundResource(movie.getImage());

        holder.icon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (clickListenerAdapter != null)
                    clickListenerAdapter.onClick(v, position);

                if(position ==0){ //go

                    holder.icon.setBackgroundResource(R.drawable.economyhighlighted0);
                    SharedPrefrences.putInt("selected",position,holder.icon.getContext());
                }

                else if(position ==1){ //rakshaw
                    if(SharedPrefrences.getInt("selected",holder.icon.getContext()) != position){

                        Resources res = holder.icon.getResources();
                        String mDrawableName = "economy" + SharedPrefrences.getInt("selected",holder.icon.getContext())+ ".png";
                        int resID = res.getIdentifier(mDrawableName , "drawable", holder.icon.getContext().getPackageName());
                        Drawable drawable = res.getDrawable(resID );
                        holder.icon.setImageDrawable(drawable);
                        //holder.icon.setImageDrawable(Drawable.createFromPath("economy" + SharedPrefrences.getInt("selected",holder.icon.getContext())+ ".png"));

                    }

                    holder.icon.setBackgroundResource(R.drawable.raskshahighlighted);
                    SharedPrefrences.putInt("selected",position,holder.icon.getContext());
                }
                else if(position ==2){ //go+
                    if(SharedPrefrences.getInt("selected",holder.icon.getContext()) != position){
                        holder.icon.setBackgroundResource(Integer.parseInt("economy" + SharedPrefrences.getInt("selected",holder.icon.getContext())+".png"));

                    }
                    holder.icon.setBackgroundResource(R.drawable.luxuryhighlighted);
                    SharedPrefrences.putInt("selected",position,holder.icon.getContext());
                }
                else if(position ==3){ //bike
                    if(SharedPrefrences.getInt("selected",holder.icon.getContext()) != position){
                        holder.icon.setBackgroundResource(Integer.parseInt("economy" + SharedPrefrences.getInt("selected",holder.icon.getContext())+".png"));

                    }
                    holder.icon.setBackgroundResource(R.drawable.bikehighlighted);
                    SharedPrefrences.putInt("selected",position,holder.icon.getContext());
                }

            }
        });
    }


    public void setClickListener(ClickListenerAdapter clickListenerAdapter) {
        this.clickListenerAdapter = clickListenerAdapter;
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}

