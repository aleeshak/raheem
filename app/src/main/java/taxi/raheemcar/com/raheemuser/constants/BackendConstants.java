package taxi.raheemcar.com.raheemuser.constants;

/**
 * Created by Aleesha Kanwal on 09/11/2017.
 */

public class BackendConstants {
    public static final String REGISTER= "api/update_new_client";
    public static final String MOBILE_NUMBER_VERIFICATION= "api/verify_new_code";
    public static final String MOBILE_NUMBER_REQUEST= "/api/get_new_mobile_number";
    public static final String LOGIN= "api/new_client_login";
    public static final String GET_DRIVERS= "api/new_free_captains";
    public static final String LOGOUT= "/apis/spur/api/logout";
    public static final String RESET_PASSWORD = "/api/reset_new_link";
    public static final String REQUEST_NEW_CAPTAIN = "/api/new_request_captain";
    public static final String UPDATED_POSITION = "/api/get_new_driver_position";
    public static final String SAVE_CAPTAIN_RATING = "/api/save_new_captain_rating";
}
