package taxi.raheemcar.com.raheemuser.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;
import com.google.firebase.FirebaseApp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.constants.Constants;
import taxi.raheemcar.com.raheemuser.models.Login;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class WelcomePasswordActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    EditText passwordTV;
    AppCompatButton submitBtn;
    String password;
    TextView forgotPasswordTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_password);
        passwordTV = (EditText) findViewById(R.id.passwordTV);
        submitBtn = (AppCompatButton) findViewById(R.id.submit);
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
        forgotPasswordTV = (TextView) findViewById(R.id.forgotPasswordTV);


        forgotPasswordTV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        passwordTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                if(!Utility.isEmpty(passwordTV.getText().toString().trim())){
                    submitBtn.setBackgroundResource(R.drawable.submit);
                    submitBtn.setEnabled(true);
                }
                else {
                    submitBtn.setBackgroundResource(R.drawable.disablesubmit);
                    submitBtn.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            login();

            }
        });
    }


    public void login() {
        if (FirebaseApp.getApps(this).isEmpty()) {
            FirebaseApp.initializeApp(this);
        }

        password = passwordTV.getText().toString().trim();
        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait ..");
        progressDialog.show();

        String mobile,token;

        mobile = SharedPrefrences.getString("phone",this);
        token = SharedPrefrences.getString(Constants.FIREBASE_TOKEN,this);

        Call<Login> response = jsonApiInterface.login(mobile,password,token);
        response.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Login serverResponse = response.body();


                if (serverResponse.getStatus()==1) {

                    progressDialog.dismiss();
                    SharedPrefrences.putBoolean("login",true,getApplicationContext());
                    SharedPrefrences.getInstance().setUserObject(serverResponse.getObject(),getApplicationContext());
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                }

                else {
                    progressDialog.dismiss();



                    SuperActivityToast.create(WelcomePasswordActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(serverResponse.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_BROWN))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                    //Utility.showOkPopUp(WelcomePasswordActivity.this,serverResponse.getMessage());

                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();


                SuperActivityToast.create(WelcomePasswordActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_BROWN))
                        .setAnimations(Style.ANIMATIONS_POP).show();

               // Utility.onServiceFailureNetworkCheck(WelcomePasswordActivity.this,t.getMessage(),InternetConnection.isNetworkAvailable(WelcomePasswordActivity.this));
                call.cancel();
            }
        });
    }
}
