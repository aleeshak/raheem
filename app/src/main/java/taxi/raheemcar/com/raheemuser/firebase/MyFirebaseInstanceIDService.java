package taxi.raheemcar.com.raheemuser.firebase;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import taxi.raheemcar.com.raheemuser.constants.Constants;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;

/**
 * Created by Aleesha Kanwal on 19/08/2018.
 */


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        SharedPrefrences.putString(Constants.FIREBASE_TOKEN,refreshedToken,this);
    }
}