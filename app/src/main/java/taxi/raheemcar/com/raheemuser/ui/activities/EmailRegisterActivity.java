package taxi.raheemcar.com.raheemuser.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;

import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class EmailRegisterActivity extends AppCompatActivity {

    AppCompatButton cont;
    EditText passwordTV;
    // Standard pattern for email
    final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_email);
        passwordTV = (EditText) findViewById(R.id.passwordTV);
        cont = (AppCompatButton) findViewById(R.id.cont);



        passwordTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                String email = passwordTV.getText().toString().trim();
                if(!Utility.isEmpty(email)) {
                    cont.setEnabled(true);
                    cont.setBackgroundResource(R.drawable.continuebutton);
                }

                else {
                    cont.setBackgroundResource(R.drawable.condisable);
                    cont.setEnabled(false);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        cont.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String email = passwordTV.getText().toString().trim();

                if (!email.matches(EMAIL_PATTERN)) {
                    SuperActivityToast.create(EmailRegisterActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText("Invalid Email Address")
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                }

                else {

                    Intent intent = new Intent(getApplicationContext(), CreatePasswordActivity.class);
                    SharedPrefrences.putString("email", email, getApplicationContext());
                    intent.putExtra("email", email);
                    startActivity(intent);

                }

            }
        });
    }
}
