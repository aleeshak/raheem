
package taxi.raheemcar.com.raheemuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Object {

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    @SerializedName("client_id")
    @Expose
    private String client_id;

    @SerializedName("GO_Per_Km_Price")
    @Expose
    private String gOPerKmPrice;
    @SerializedName("GO_Basic_Price")
    @Expose
    private String gOBasicPrice;
    @SerializedName("Partner/Raksha_Per_Km_Price")
    @Expose
    private String partnerRakshaPerKmPrice;
    @SerializedName("Partner/Raksha_Basic_Price")
    @Expose
    private String partnerRakshaBasicPrice;
    @SerializedName("Bike_Per_Km_Price")
    @Expose
    private String bikePerKmPrice;
    @SerializedName("Bike_Basic_Price")
    @Expose
    private String bikeBasicPrice;
    @SerializedName("GO Plus_Per_Km_Price")
    @Expose
    private String gOPlusPerKmPrice;
    @SerializedName("GO Plus_Basic_Price")
    @Expose
    private String gOPlusBasicPrice;

    public String getGOPerKmPrice() {
        return gOPerKmPrice;
    }

    public void setGOPerKmPrice(String gOPerKmPrice) {
        this.gOPerKmPrice = gOPerKmPrice;
    }

    public String getGOBasicPrice() {
        return gOBasicPrice;
    }

    public void setGOBasicPrice(String gOBasicPrice) {
        this.gOBasicPrice = gOBasicPrice;
    }

    public String getPartnerRakshaPerKmPrice() {
        return partnerRakshaPerKmPrice;
    }

    public void setPartnerRakshaPerKmPrice(String partnerRakshaPerKmPrice) {
        this.partnerRakshaPerKmPrice = partnerRakshaPerKmPrice;
    }

    public String getPartnerRakshaBasicPrice() {
        return partnerRakshaBasicPrice;
    }

    public void setPartnerRakshaBasicPrice(String partnerRakshaBasicPrice) {
        this.partnerRakshaBasicPrice = partnerRakshaBasicPrice;
    }

    public String getBikePerKmPrice() {
        return bikePerKmPrice;
    }

    public void setBikePerKmPrice(String bikePerKmPrice) {
        this.bikePerKmPrice = bikePerKmPrice;
    }

    public String getBikeBasicPrice() {
        return bikeBasicPrice;
    }

    public void setBikeBasicPrice(String bikeBasicPrice) {
        this.bikeBasicPrice = bikeBasicPrice;
    }

    public String getGOPlusPerKmPrice() {
        return gOPlusPerKmPrice;
    }

    public void setGOPlusPerKmPrice(String gOPlusPerKmPrice) {
        this.gOPlusPerKmPrice = gOPlusPerKmPrice;
    }

    public String getGOPlusBasicPrice() {
        return gOPlusBasicPrice;
    }

    public void setGOPlusBasicPrice(String gOPlusBasicPrice) {
        this.gOPlusBasicPrice = gOPlusBasicPrice;
    }

}
