package taxi.raheemcar.com.raheemuser.util;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * @author VentureDive/Ammar Amjad
 */
public class CustomTextWatcher implements TextWatcher {

    protected int myPosition;

    public CustomTextWatcher(int myVariable) {
        this.myPosition = myVariable;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
