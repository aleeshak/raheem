
package taxi.raheemcar.com.raheemuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Updatedposition {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("object")
    @Expose
    private PositionObject object;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PositionObject getObject() {
        return object;
    }

    public void setObject(PositionObject object) {
        this.object = object;
    }

}
