package taxi.raheemcar.com.raheemuser.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import taxi.raheemcar.com.raheemuser.App;
import taxi.raheemcar.com.raheemuser.R;


public class MessageDialog {

    public static final String TAG = MessageDialog.class.getSimpleName();

    private AlertDialog.Builder builder;
    private AlertDialog dialog;

    Activity activity;

    String dialogLabelText = null;
    View dialogCustomView = null;
    SpannableStringBuilder dialogLabelTextSpannable = null;
    String dialogOkBtnText = null;
    String dialogNoBtnText = null;
    String dialogYesBtnText = null;

    TextView dialogLabel;
    public Button okButton;
    public Button noButton;
    public Button yesButton;

    private boolean isSpannable = false;
    private boolean isView = false;

    private String title = "";
    private boolean optionTextSizeSmall = false;

    public MessageDialog(Activity a, int label, int oklabel, int nolabel, int yeslabel) {
        activity = a;

        if (label != -1)
            dialogLabelText = activity.getResources().getString(label);

        if (oklabel != -1)
            dialogOkBtnText = activity.getResources().getString(oklabel);

        if (nolabel != -1)
            dialogNoBtnText = activity.getResources().getString(nolabel);

        if (yeslabel != -1)
            dialogYesBtnText = activity.getResources().getString(yeslabel);

        createBuilder();
    }

    public MessageDialog(Activity a, View customView, int oklabel, int nolabel, int yeslabel) {
        activity = a;
        this.isView = true;
        dialogCustomView = customView;
//        if (label != -1)
//            dialogLabelText = activity.getResources().getString(label);

        if (oklabel != -1)
            dialogOkBtnText = activity.getResources().getString(oklabel);

        if (nolabel != -1)
            dialogNoBtnText = activity.getResources().getString(nolabel);

        if (yeslabel != -1)
            dialogYesBtnText = activity.getResources().getString(yeslabel);

        createBuilder();
    }

    public MessageDialog(Activity a, String label, int oklabel, int nolabel, int yeslabel) {
        activity = a;

        if (label != null && !label.isEmpty())
            dialogLabelText = label;
        else
            dialogLabelText = activity.getString(R.string.error_general);

        if (oklabel != -1)
            dialogOkBtnText = App.getAppContext().getResources().getString(oklabel);

        if (nolabel != -1)
            dialogNoBtnText = App.getAppContext().getResources().getString(nolabel);

        if (yeslabel != -1)
            dialogYesBtnText = App.getAppContext().getResources().getString(yeslabel);

        createBuilder();
    }

    public MessageDialog(Activity a, SpannableStringBuilder label, int oklabel, int nolabel, int yeslabel) {
        activity = a;


        if (label != null && !label.toString().isEmpty()) {
            isSpannable = true;
            dialogLabelTextSpannable = label;
        } else {
            dialogLabelText = activity.getString(R.string.error_general);
        }

        if (oklabel != -1)
            dialogOkBtnText = activity.getResources().getString(oklabel);

        if (nolabel != -1)
            dialogNoBtnText = activity.getResources().getString(nolabel);

        if (yeslabel != -1)
            dialogYesBtnText = activity.getResources().getString(yeslabel);

        createBuilder();
    }

    public MessageDialog(Activity a, String label, int oklabel, int nolabel, int yeslabel, String title, boolean optionTextSizeSmall) {
        activity = a;
        this.title = title;
        this.optionTextSizeSmall = optionTextSizeSmall;

        if (label != null && !label.isEmpty())
            dialogLabelText = label;
        else
            dialogLabelText = activity.getString(R.string.error_general);


        if (oklabel != -1)
            dialogOkBtnText = activity.getResources().getString(oklabel);

        if (nolabel != -1)
            dialogNoBtnText = activity.getResources().getString(nolabel);

        if (yeslabel != -1)
            dialogYesBtnText = activity.getResources().getString(yeslabel);

        createBuilder();
    }


    public void createBuilder() {

        if (Utility.getDeviceOsVersion() >= Build.VERSION_CODES.LOLLIPOP) {
            if (dialogOkBtnText != null) dialogOkBtnText = dialogOkBtnText.toUpperCase();
            if (dialogNoBtnText != null) dialogNoBtnText = dialogNoBtnText.toUpperCase();
            if (dialogYesBtnText != null) dialogYesBtnText = dialogYesBtnText.toUpperCase();
        }

        builder = new AlertDialog.Builder(activity);

        if (!Utility.isEmpty(title))
            builder.setTitle(title);

        if (!isSpannable) {
            if (!isView) {
                if (optionTextSizeSmall) {
                    builder.setMessage(Html.fromHtml(activity.getResources().getString(R.string.rate_now_text)));
                } else
                    builder.setMessage(dialogLabelText);
            } else {
                builder.setView(dialogCustomView);
            }
        } else {
            builder.setMessage(dialogLabelTextSpannable);
        }
        builder.setCancelable(false);

        if (dialogOkBtnText != null && dialogYesBtnText == null) {
            builder.setPositiveButton(dialogOkBtnText, null);

        } else if (dialogOkBtnText == null && dialogYesBtnText != null) {
            builder.setPositiveButton(dialogYesBtnText, null);

        } else if (dialogOkBtnText != null) {
            builder.setPositiveButton(dialogYesBtnText, null);
            builder.setNeutralButton(dialogOkBtnText, null);
        }

        if (dialogYesBtnText != null) {
            builder.setPositiveButton(dialogYesBtnText, null);
        }
        if (dialogNoBtnText != null) {
            builder.setNegativeButton(dialogNoBtnText, null);
        }

        dialog = builder.create();
    }

    public void show() {
        dialog.show();
        noButton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        noButton.setTextColor(activity.getResources().getColor(R.color.New_RED));

        if (dialogOkBtnText != null && dialogYesBtnText == null) {
            okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            yesButton = dialog.getButton(DialogInterface.BUTTON_NEUTRAL);
            okButton.setTextColor(activity.getResources().getColor(R.color.New_RED));
            yesButton.setTextColor(activity.getResources().getColor(R.color.New_RED));


        } else if (dialogOkBtnText == null && dialogYesBtnText != null) {
            okButton = dialog.getButton(DialogInterface.BUTTON_NEUTRAL);
            yesButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            okButton.setTextColor(activity.getResources().getColor(R.color.New_RED));
            yesButton.setTextColor(activity.getResources().getColor(R.color.New_RED));


        } else if (dialogOkBtnText != null) {
            okButton = dialog.getButton(DialogInterface.BUTTON_NEUTRAL);
            yesButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            okButton.setTextColor(activity.getResources().getColor(R.color.New_RED));
            yesButton.setTextColor(activity.getResources().getColor(R.color.New_RED));

        }

        if (optionTextSizeSmall) {
            yesButton.setTextColor(activity.getResources().getColor(R.color.New_RED));
            okButton.setTextColor(activity.getResources().getColor(R.color.New_GREY));
            noButton.setTextColor(activity.getResources().getColor(R.color.New_GREY));
            noButton.setPadding(0,0,110,0);
        }
    }

    public void dismiss() {
        dialog.dismiss();
    }

    public void setCancelable(boolean flag) {
        dialog.setCancelable(flag);
    }

    public void cancel() {
        dialog.cancel();
    }

    public Window getWindow() {
        return dialog.getWindow();
    }
}
