
package taxi.raheemcar.com.raheemuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PositionObject {

    @SerializedName("position_id")
    @Expose
    private String positionId;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("time")
    @Expose
    private String time;

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
