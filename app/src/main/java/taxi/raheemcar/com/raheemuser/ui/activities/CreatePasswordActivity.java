package taxi.raheemcar.com.raheemuser.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class CreatePasswordActivity extends AppCompatActivity {

    EditText passwordTV;
    AppCompatButton cont;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);
        passwordTV = (EditText) findViewById(R.id.passwordTV);

        cont = (AppCompatButton) findViewById(R.id.cont);

        cont.setEnabled(false);


        passwordTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                if(!Utility.isEmpty(passwordTV.getText().toString().trim())){
                    cont.setBackgroundResource(R.drawable.continuebutton);
                    cont.setEnabled(true);
                }
                else {
                    cont.setBackgroundResource(R.drawable.condisable);
                    cont.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        cont.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String password = passwordTV.getText().toString().trim();
                if(password.length() > 5) {
                    Intent intent = new Intent(getApplicationContext(), NameActivity.class);
                    SharedPrefrences.putString("password", password, getApplicationContext());
                    intent.putExtra("password", password);
                    startActivity(intent);
                }
                else {
                    SuperActivityToast.create(CreatePasswordActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText("Password must be in 6 letters")
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();
                }

            }
        });
    }
}
