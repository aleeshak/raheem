package taxi.raheemcar.com.raheemuser.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.ui.activities.MainActivity;
import taxi.raheemcar.com.raheemuser.ui.activities.NotificationActivity;
import taxi.raheemcar.com.raheemuser.ui.activities.RateCaptainActivity;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 19/08/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static int NOTIFICATION_ID = 1;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        sendNotification(remoteMessage.getData());
    }

    private void sendNotification(Map<String, String> data) {

        Log.e("notfy", String.valueOf(data.size()));
       if (data.size()==3){
            int num = ++NOTIFICATION_ID;
            Bundle msg = new Bundle();

            for (String key : data.keySet()) {
                // Log.e("sadeeq", data.get(key));
                msg.putString(key, data.get(key));
            }

            Intent intent = new Intent(this, MainActivity.class);
            if (msg.containsKey("action")) {
                intent.putExtra("action", msg.getString("action"));
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, num , intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.icon)
                    .setContentTitle(msg.getString("title"))
                    .setContentText(msg.getString("message"))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(num, notificationBuilder.build());
        }

        else if (data.size()==2){

           SharedPrefrences.putBoolean("notif",true,getApplicationContext());

            int num = ++NOTIFICATION_ID;
            Bundle msg = new Bundle();

            for (String key : data.keySet()) {
                msg.putString(key, data.get(key));
            }

            Intent intent = new Intent(this, NotificationActivity.class);
            if (msg.containsKey("action")) {
                intent.putExtra("action", msg.getString("action"));
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, num , intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(msg.getString("title"))
                    .setContentText(msg.getString("message"))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(num, notificationBuilder.build());
        }
        else
        {

            SharedPrefrences.putBoolean("ride",false,getApplicationContext());

            int num = ++NOTIFICATION_ID;
            Bundle msg = new Bundle();

            for (String key : data.keySet()) {
                // Log.e("sadeeq", data.get(key));
                msg.putString(key, data.get(key));
            }

            Intent intent = new Intent(this, RateCaptainActivity.class);
            if (msg.containsKey("action")) {
                intent.putExtra("action", msg.getString("action"));
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, num , intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(msg.getString("title"))
                    .setContentText(msg.getString("message"))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            notificationManager.notify(num, notificationBuilder.build());


            if(Utility.isAppRunning(this,getApplicationContext().getPackageName())){
                Intent intent1 = new Intent(this, RateCaptainActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                startActivity(intent1);
            }

        }
    }

    private void showNotification(String message) {

        Intent i = new Intent(this,MyFirebaseMessagingService.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("FCM Test")
                .setContentText(message)
                .setSound(defaultSoundUri)
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(0,builder.build());
    }


}