package taxi.raheemcar.com.raheemuser.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.models.MobileNo;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.MessageDialog;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class MobileNumberActivity extends AppCompatActivity {

    EditText mobileNumberET;
    AppCompatButton cont;
    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobileno);

        mobileNumberET = (EditText) findViewById(R.id.mobileNumberET);
        cont = (AppCompatButton) findViewById(R.id.cont);
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
        phone = mobileNumberET.getText().toString().trim();
        cont.setEnabled(false);

        mobileNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                if((mobileNumberET.getText().toString().startsWith("+92") || mobileNumberET.getText().toString().startsWith("033")) && mobileNumberET.getText().toString().length() > 9){
                    cont.setBackgroundResource(R.drawable.continuebutton);
                    cont.setEnabled(true);
                }
                else {
                    cont.setBackgroundResource(R.drawable.condisable);
                    cont.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        cont.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showAlertPopup("is this your mobile number ? \n" + mobileNumberET.getText().toString().trim());

            }
        });

    }


    public void showAlertPopup(String message) {
        final MessageDialog dialog = new MessageDialog(this, message,
                -1, R.string.dialog_edit_no_text, R.string.dialog_edit_yes_text);
        dialog.setCancelable(false);
        if (!isFinishing()) {
            dialog.show();
            dialog.yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                    phone = mobileNumberET.getText().toString().trim();
                    if(Utility.isPhoneNumberValid(phone)) {
                        Utility.hideKeyboardFrom(mobileNumberET,getApplicationContext());
                        registerNumber();
                    }
                    else {
                        Utility.hideKeyboardFrom(mobileNumberET,getApplicationContext());
                        SuperActivityToast.create(MobileNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                                .setProgressBarColor(Color.WHITE)
                                .setText("Please enter valid phone number")
                                .setDuration(Style.DURATION_LONG)
                                .setFrame(Style.FRAME_LOLLIPOP)
                                .setColor(getResources().getColor(R.color.colorPrimary))
                                .setAnimations(Style.ANIMATIONS_POP).show();
                    }
                }
            });

            dialog.noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
        }
    }

    public void registerNumber() {
        phone = mobileNumberET.getText().toString().trim();
        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<MobileNo> response = jsonApiInterface.resgiterMobileNumber(phone);
        response.enqueue(new Callback<MobileNo>() {
            @Override
            public void onResponse(Call<MobileNo> call, Response<MobileNo> response) {
                MobileNo serverResponse = response.body();

                SharedPrefrences.putString("phone",phone,getApplicationContext());
                if (serverResponse.getStatus()==1) {

                    int code = serverResponse.getCode();
                    SharedPrefrences.putInt("code",code,getApplicationContext());
                    progressDialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(),VerifyNumberActivity.class);
                    startActivity(intent);


                }
                else if(serverResponse.getStatus() == 0){
                    progressDialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(),WelcomePasswordActivity.class);
                    startActivity(intent);
                }


                else {
                    progressDialog.dismiss();

                    SuperActivityToast.create(MobileNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(serverResponse.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();


              //     Utility.showOkPopUp(MobileNumberActivity.this,serverResponse.getMessage());


                }
            }

            @Override
            public void onFailure(Call<MobileNo> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();

                SuperActivityToast.create(MobileNumberActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
               // Utility.onServiceFailureNetworkCheck(MobileNumberActivity.this,t.getMessage(),InternetConnection.isNetworkAvailable(MobileNumberActivity.this));
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }
}
