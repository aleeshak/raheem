package taxi.raheemcar.com.raheemuser.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.constants.Constants;
import taxi.raheemcar.com.raheemuser.models.Rating;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 10/10/2018.
 */
public class RateCaptainActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    AppCompatButton finish;
    CircleImageView profile_image;
    TextView driverName;
    RatingBar driverRating;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_captain);
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);

        String imageName = SharedPrefrences.getInstance().getDriverObject().getImage();

        profile_image = (CircleImageView) findViewById(R.id.profile_image);

        driverName = (TextView) findViewById(R.id.driverName);

        String name = SharedPrefrences.getInstance().getDriverObject().getDriverName();

        driverName.setText("how was your ride with " + name);

        Picasso.with(this).load(Constants.IMAGE_URL + imageName).placeholder(R.drawable.driver).into(profile_image);

        finish = (AppCompatButton) findViewById(R.id.finish);



        finish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                driverRating = (RatingBar) findViewById(R.id.driverRating);
                if(driverRating.getRating()!=0){
                    saveCaptainRating();
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                }
                else {
                    SuperActivityToast.create(RateCaptainActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText("Please select rating")
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();
                }

            }
        });


    }


    public void saveCaptainRating() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));
        driverRating = (RatingBar) findViewById(R.id.driverRating);
        String driverID = SharedPrefrences.getInstance().getDriverObject().getDriverId();

        Call<Rating> response = jsonApiInterface.saveCaptainRating(driverRating.getRating(),driverID);
        response.enqueue(new Callback<Rating>() {
            @Override
            public void onResponse(Call<Rating> call, Response<Rating> response) {
                Rating serverResponse = response.body();

                    progressDialog.cancel();

                    SuperActivityToast.create(RateCaptainActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(serverResponse.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();



            }

            @Override
            public void onFailure(Call<Rating> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();
                SuperActivityToast.create(RateCaptainActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

                call.cancel();
            }
        });
    }

}
