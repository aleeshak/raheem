package taxi.raheemcar.com.raheemuser.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.models.Reset;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class ForgotPasswordActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    EditText passwordTV;
    String email;
    AppCompatButton submitBtn;
    final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);
        passwordTV = (EditText) findViewById(R.id.passwordTV);
        submitBtn = (AppCompatButton) findViewById(R.id.submitBtn);
        submitBtn.setEnabled(false);

        passwordTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                String email = passwordTV.getText().toString().trim();
                if(!Utility.isEmpty(email)) {
                    submitBtn.setEnabled(true);
                    submitBtn.setBackgroundResource(R.drawable.resent);
                }

                else {
                    submitBtn.setBackgroundResource(R.drawable.resentdisable);
                    submitBtn.setEnabled(false);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });



        submitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                String email = passwordTV.getText().toString().trim();

                if (!email.matches(EMAIL_PATTERN)) {
                    SuperActivityToast.create(ForgotPasswordActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText("Invalid Email Address")
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                }

                else {
                    resetPassword();
                }

            }
        });
    }


    public void resetPassword() {
        email = passwordTV.getText().toString().trim();
        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<Reset> response = jsonApiInterface.resetPassword(email);
        response.enqueue(new Callback<Reset>() {
            @Override
            public void onResponse(Call<Reset> call, Response<Reset> response) {
                Reset serverResponse = response.body();

                progressDialog.dismiss();

                SuperActivityToast.create(ForgotPasswordActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(serverResponse.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();
            }

            @Override
            public void onFailure(Call<Reset> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();

                SuperActivityToast.create(ForgotPasswordActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

                call.cancel();
            }
        });
    }
}
