package taxi.raheemcar.com.raheemuser.ui.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import taxi.raheemcar.com.raheemuser.R;
import taxi.raheemcar.com.raheemuser.constants.Constants;
import taxi.raheemcar.com.raheemuser.models.NewCaptain;
import taxi.raheemcar.com.raheemuser.models.POJO.Example;
import taxi.raheemcar.com.raheemuser.networking.JsonApiInterface;
import taxi.raheemcar.com.raheemuser.networking.RetrofitClient;
import taxi.raheemcar.com.raheemuser.sharedprefrences.SharedPrefrences;
import taxi.raheemcar.com.raheemuser.util.GPSTracker;
import taxi.raheemcar.com.raheemuser.util.InternetConnection;
import taxi.raheemcar.com.raheemuser.util.MessageDialog;
import taxi.raheemcar.com.raheemuser.util.Utility;

/**
 * Created by Aleesha Kanwal on 10/09/2018.
 */
public class DropoffActivity extends AppCompatActivity implements OnMapReadyCallback {

    PlaceAutocompleteFragment placeAutoComplete;
    String destinationLocation;
    Polyline line;
    LatLng destinationLatLng;
    TextView duration;
    private GoogleMap mMap;
    GPSTracker gpsTracker;
    double latitude, longitude;
    ArrayList<LatLng> MarkerPoints;
    AppCompatButton addDrop, skip;
    ProgressDialog progressDialog;
    JsonApiInterface jsonApiInterface;
    String currentLocationStreet;
    String timee;
    int dist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropup);

        duration = (TextView) findViewById(R.id.duration);
        addDrop = (AppCompatButton) findViewById(R.id.addDrop);
        skip = (AppCompatButton) findViewById(R.id.skip);
        placeAutoComplete = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete);
        jsonApiInterface = RetrofitClient.getClient().create(JsonApiInterface.class);

        permissionCheckAndCallDistanceAPI();
        getCurrentLocationAddress();

        MarkerPoints = new ArrayList<>();

        addDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(destinationLatLng!=null) {
                    int price = 1;

                    if(SharedPrefrences.getString(Constants.SELECTED_VEHICLE_TYPE,getApplicationContext()).equals(Constants.VEHICLE_ID_BIKE)){
                        price = Integer.valueOf(SharedPrefrences.getInstance().getUserObject().getBikeBasicPrice()) + Integer.valueOf(SharedPrefrences.getInstance().getUserObject().getBikePerKmPrice()) * dist;
                    }

                   else if(SharedPrefrences.getString(Constants.SELECTED_VEHICLE_TYPE,getApplicationContext()).equals(Constants.VEHICLE_ID_GO)){
                         price = Integer.valueOf(SharedPrefrences.getInstance().getUserObject().getGOBasicPrice()) + Integer.valueOf(SharedPrefrences.getInstance().getUserObject().getGOPerKmPrice()) * dist;
                    }

                   else if(SharedPrefrences.getString(Constants.SELECTED_VEHICLE_TYPE,getApplicationContext()).equals(Constants.VEHICLE_ID_GO_PLUS)){
                         price = Integer.valueOf(SharedPrefrences.getInstance().getUserObject().getGOPlusBasicPrice()) + Integer.valueOf(SharedPrefrences.getInstance().getUserObject().getGOPlusPerKmPrice()) * dist;
                    }

                   else if(SharedPrefrences.getString(Constants.SELECTED_VEHICLE_TYPE,getApplicationContext()).equals(Constants.VEHICLE_ID_RAKSHAW)){
                         price = Integer.valueOf(SharedPrefrences.getInstance().getUserObject().getPartnerRakshaBasicPrice()) + Integer.valueOf(SharedPrefrences.getInstance().getUserObject().getPartnerRakshaPerKmPrice()) * dist;
                    }

                    showAlertPopup("Total Distance is = " +dist + " KM \n" + "Estimated cost for this ride is =" + price) ;
                    //requestNewCaptain(destinationLatLng.latitude,destinationLatLng.longitude,destinationLocation);
                }
                else {
                    Utility.showOkPopUp(DropoffActivity.this,"Please select your destination");
                }
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String guideCaptain = "I will guide the Captain";
                requestNewCaptain(latitude,longitude,guideCaptain);
            }
        });


        placeAutoComplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                destinationLocation = (String) place.getName();
                destinationLatLng = place.getLatLng();

                mMap.clear();
                addLocationMarker();


                // Adding new item to the ArrayList
                // MarkerPoints.add(place.getLatLng());

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(destinationLatLng.latitude, destinationLatLng.longitude))
                        .title("Destinaion")
                        .snippet("There you go")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                // .icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi)));


                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16.5f));

                build_retrofit_and_get_response("driving");

                Log.d("Maps", "Place selected: " + destinationLocation);
            }

            @Override
            public void onError(Status status) {
                Log.d("Maps", "An error occurred: " + status);
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        addLocationMarker();

    }

    private void build_retrofit_and_get_response(String type) {

        String url = "https://maps.googleapis.com/maps/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonApiInterface service = retrofit.create(JsonApiInterface.class);

        Call<Example> call = service.getDistanceDuration("metric", latitude + "," + longitude, destinationLatLng.latitude + "," + destinationLatLng.longitude, type);

        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                try {
                    //Remove previous line from map
                    if (line != null) {
                        line.remove();
                    }
                    // This loop will go through all the results and add marker on each location.
                    for (int i = 0; i < response.body().getRoutes().size(); i++) {
                        int distance = response.body().getRoutes().get(i).getLegs().get(i).getDistance().getValue();

                        String time = response.body().getRoutes().get(i).getLegs().get(i).getDuration().getText();
                        duration.setVisibility(View.VISIBLE);
                        dist = distance/1000;
                        timee = time;

                        duration.setText("Distance:" + dist + ", Duration:" + time);

                        String encodedString = response.body().getRoutes().get(0).getOverviewPolyline().getPoints();
                        List<LatLng> list = decodePoly(encodedString);
                        line = mMap.addPolyline(new PolylineOptions()
                                .addAll(list)
                                .width(20)
                                .color(Color.RED)
                                .geodesic(true)
                        );
                    }
                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });

    }

    public void addLocationMarker() {

        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        LatLng currentLocation = new LatLng(latitude, longitude);

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title("You")
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.raheemmap)));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.5f));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;


        }
    }


    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    // Checking if Google Play Services Available or not
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    public void permissionCheckAndCallDistanceAPI() {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        } else {

            if (destinationLatLng != null && latitude != 0 && longitude != 0) {
                build_retrofit_and_get_response("driving");
                Log.e("distance api", "called");
            }

            Log.e("distance api", "not called");
        }

        //show error dialog if Google Play Services not available
        if (!isGooglePlayServicesAvailable()) {
            Log.d("onCreate", "Google Play Services not available. Ending Test case.");
            finish();
        } else {
            Log.d("onCreate", "Google Play Services available. Continuing.");
        }

        gpsTracker = new GPSTracker(getApplicationContext());

        if (gpsTracker.canGetLocation()) {

            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

        } else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            showSettingsAlert();
        }
    }


    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DropoffActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS settings");

        // Setting Dialog Message
        alertDialog.setMessage("Raheem would like to use your current location and GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();


            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void getCurrentLocationAddress() {
        Geocoder geocoder = new Geocoder(getApplicationContext());
        try {
            List<Address> addresses = null;
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                // Thoroughfare seems to be the street name without numbers
                currentLocationStreet = address.getThoroughfare();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestNewCaptain(double lat, double lng,String destinationLocation) {

        String vehicleId = SharedPrefrences.getString(Constants.SELECTED_VEHICLE_TYPE, getApplicationContext());
        String cliendId = SharedPrefrences.getInstance().getUserObject().getClient_id();
        Utility.onServiceSuccessNetworkCheck(this, InternetConnection.isNetworkAvailable(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        Call<NewCaptain> response = jsonApiInterface.requestNewCaptain(lat, lng, vehicleId, cliendId, currentLocationStreet, destinationLocation);
        response.enqueue(new Callback<NewCaptain>() {
            @Override
            public void onResponse(Call<NewCaptain> call, Response<NewCaptain> response) {
                NewCaptain serverResponse = response.body();

                progressDialog.dismiss();

                if(serverResponse.getStatus()==1){
                    SharedPrefrences.getInstance().setDriverObject(serverResponse.getObject(),getApplicationContext());

                    SuperActivityToast.create(DropoffActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(serverResponse.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                    //sendNotification();

                    Intent intent = new Intent(getApplicationContext(),NotificationActivity.class);
                    startActivity(intent);

                }

                else {

                    SuperActivityToast.create(DropoffActivity.this, new Style(), Style.TYPE_BUTTON)
                            .setProgressBarColor(Color.WHITE)
                            .setText(serverResponse.getMessage())
                            .setDuration(Style.DURATION_LONG)
                            .setFrame(Style.FRAME_LOLLIPOP)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setAnimations(Style.ANIMATIONS_POP).show();

                }


            }

            @Override
            public void onFailure(Call<NewCaptain> call, Throwable t) {
                t.getMessage();
                progressDialog.cancel();

                SuperActivityToast.create(DropoffActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText(t.getMessage())
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAnimations(Style.ANIMATIONS_POP).show();

                call.cancel();
            }
        });
    }

    public void sendNotification() {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.logo);
        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra(getApplicationContext().getPackageName(), 1);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        builder.setContentIntent(pendingIntent);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setContentTitle("Raheem");
        builder.setAutoCancel(true);
        builder.setContentText("You have booked a ride");
        builder.setSubText("Tap to view");

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Will display the notification in the notification bar
        notificationManager.notify(1, builder.build());
    }


    public void showAlertPopup(String message) {
        final MessageDialog dialog = new MessageDialog(this, message,
                -1, R.string.dialog_edit_no_text, R.string.dialog_edit_yes_text);
        dialog.setCancelable(false);
        if (!isFinishing()) {
            dialog.show();
            dialog.yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();

                    if (destinationLatLng != null) {
                        requestNewCaptain(latitude,longitude, destinationLocation);
                    }
                }
            });

            dialog.noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
        }
    }
}
