package taxi.raheemcar.com.raheemuser.models;

/**
 * Created by Aleesha Kanwal on 15/08/2018.
 */

public class Vehicles {


    private String title;
    private int image;

    public Vehicles(){}

    public Vehicles(String title,int image){
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
